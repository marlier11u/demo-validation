package net.tncy.marlier11u.demo;


import net.tncy.marlier11u.demo.AdultCheck;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.Date;

public class Person {

    @NotNull
    @Size(min=1)
    private String firstName;

    @NotNull
    @Size(min=1)
    private String lastName;

    @NotNull(groups = {AdultCheck.class})
    private Date birthDate;

    private String cityzenship;

    @NotNull(groups = {AdultCheck.class})
    @Min(value=18, groups = {AdultCheck.class})
    private transient Integer age;

    public Person(String fn,String ln, Date d, String c){
        firstName=fn;
        lastName=ln;
        birthDate=d;
        cityzenship=c;

        Calendar calendar = Calendar.getInstance();
        int curr=calendar.get(Calendar.YEAR);
        calendar.setTime(birthDate);
        int b=calendar.get(Calendar.YEAR);
        age=curr-b;


    }

}