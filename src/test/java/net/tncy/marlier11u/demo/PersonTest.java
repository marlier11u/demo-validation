package net.tncy.marlier11u.demo;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.*;

public class PersonTest {

    private static Validator validator;

    @BeforeClass
    public static  void setUpClass(){
        ValidatorFactory factory= Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validateAdult(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(1995, Calendar.JULY, 26);
        Date birthDate = calendar.getTime();
        Person p =new Person("Romain", "MARLIER", birthDate, "FR");
        Set<ConstraintViolation<Person>> constraintViolationSet = validator.validate(p);
        assertEquals(0, constraintViolationSet.size());
    }

}